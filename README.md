# topic05-resources-imageswap
# ImageSwap
Sample code using resources  with various atributes
drawables
imagebuttons
colours
raw (sound, MediaPlayer)
strings (links clickable etc)
dimensions

Uses Toasts

## Note

There are a lot more widgets and resources 

* https://developer.android.com/guide/topics/resources/providing-resources
* https://developer.android.com/guide/topics/resources/complex-xml-resources

See Also resource types, animation, colour etc for details 


